define(
	[ 'collections/artists', 'libs/text/text!/templates/artists/list.html' ],
	function (Artists, artistListTemplate) {
		var ArtistListView = Backbone.View.extend({
			initialize: function() {
				this.collection = new Artists();
			},

			render: function(fetch) {
				if (fetch) this.collection.fetch({
					success: _.bind(function() {
						$(this.el).html(_.template(artistListTemplate, {artists:this.collection.models}));
					}, this)
				});
			}
		});

		return ArtistListView;
	}
);

